/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 */

//// The AST

// This class represents all AST Nodes.
function ASTNode(type) {
    this.type = type;
}

ASTNode.prototype = {};

// The All node just outputs all records.
function AllNode() {
    ASTNode.call(this, "All");
}

// This is how we make AllNode subclass from ASTNode.
AllNode.prototype = Object.create(ASTNode.prototype);

// The Filter node uses a callback to throw out some records.
function FilterNode(callback) {
    ASTNode.call(this, "Filter");
    this.callback = callback;
}

FilterNode.prototype = Object.create(ASTNode.prototype);

// The Then node chains multiple actions on one data structure.
function ThenNode(first, second) {
    ASTNode.call(this, "Then");
    this.first = first;
    this.second = second;
}

ThenNode.prototype = Object.create(ASTNode.prototype);




//// Executing queries

ASTNode.prototype.execute = function(table) {
    throw new Error("Unimplemented AST node " + this.type)
}

// ...
AllNode.prototype.execute = function(table) {
  return table.slice();
}

FilterNode.prototype.execute = function(table) {
  return table.filter(this.callback);
}

ThenNode.prototype.execute = function(table) {
  return this.second.execute(this.first.execute(table));
}

//// Write a query
// Define the `thefts_query` and `auto_thefts_query` variables

var thefts_query = new FilterNode(function(row) {
                                    return row[13].match(/THEFT/);
                                  });

var auto_thefts_query = new FilterNode(function(row) {
                                    return row[13].match(/^VEH-THEFT/);
                                  });

//// Add Apply and Count nodes

// ...
ApplyNode.prototype = Object.create(ASTNode.prototype);

function ApplyNode(method) {
  ASTNode.call(this, "Apply");
  this.method = method;
}

ApplyNode.prototype.execute = function(table) {
  return table.map(this.method);
}

CountNode.prototype = Object.create(ASTNode.prototype);

function CountNode() {
  ASTNode.call(this, "Count");
}

CountNode.prototype.execute = function(table) {
  return [table.length];
}



//// Clean the data

var cleanup_query = new ApplyNode(function(row) {
                                    return {
                                      type: row[13],
                                      description: row[15],
                                      date: row[17],
                                      area: row[18]
                                    }
                                  });


//// Implement a call-chaining interface

// ...

Q = Object.create(new AllNode());

ASTNode.prototype.filter = function(callback) {
  return new ThenNode(this, new FilterNode(callback));
}

ASTNode.prototype.apply = function(method) {
  return new ThenNode(this, new ApplyNode(method));
}

ASTNode.prototype.count = function() {
  return new ThenNode(this, new CountNode());
}

//// Reimplement queries with call-chaining

var cleanup_query_2 = Q.apply(function(row) {;
                                return {
                                  type: row[13],
                                  description: row[15],
                                  date: row[17],
                                  area: row[18]
                                }
                              });


var thefts_query_2 = Q.filter(function(row) {
                                return row.type.match(/THEFT/);
                              });

var auto_thefts_query_2 = Q.filter(function(row) {
                                    return row.type.match(/^VEH-THEFT/);
                                  });


//// Optimize filters

ASTNode.prototype.optimize = function() { return this; }

ThenNode.prototype.optimize = function() {
    return new ThenNode(this.first.optimize(), this.second.optimize())
}

// We add a "run" method that is like "execute" but optimizes queries first.

ASTNode.prototype.run = function(data) {
    this.optimize().execute(data);
}

function AddOptimization(node_type, f) {
    var old = node_type.prototype.optimize;
    node_type.prototype.optimize = function() {
        var new_this = old.apply(this, arguments);
        return f.apply(new_this, arguments) || new_this;
    }
}

// ...
AddOptimization(ThenNode, function() {
  if (this.first.type == "Then"
      && this.second.type == "Filter"
      && this.first.second.type == "Filter") {
       var f = this.second.callback;
       var g = this.first.second.callback;
       var x = this.first.first;
       this.first = x;
       this.second = new FilterNode(function(row) {
         return f(row) && g(row);
       });
  }
  return this;
});





//// Internal node types and CountIf

// ...
CountIfNode.prototype = Object.create(ASTNode.prototype);

function CountIfNode(callback) {
    ASTNode.call(this, "CountIf");
    this.callback = callback;
}

CountIfNode.prototype.execute = function(table) {
  return table.filter(this.callback).length;
}

AddOptimization(ThenNode, function() {
  if (this.first.type == "Then"
      && this.second.type == "Count"
      && this.first.second.type == "Filter") {
        var x = this.first.first;
        var f =this.first.second.callback;
        this.first = x;
        this.second = new CountIfNode(f);
  }
  return this;
});


//// Cartesian Products

// ...
CartesianProductNode.prototype = Object.create(ASTNode.prototype);

function CartesianProductNode(left, right) {
  ASTNode.call(this, "CartesianProduct");
  this.left = left;
  this.right = right;
}

// Reused again on hashJoin
function product(result, left, right) {
    left.forEach(function(leftRow) {
      right.forEach(function(rightRow) {
        result.push({
          left: leftRow,
          right: rightRow
        });
      });
    });
}

CartesianProductNode.prototype.execute = function(table) {
    var result = [];
    var left = this.left.execute(table);
    var right = this.right.execute(table);
    product(result, left, right);
    return result;
}


CartesianProductNode.prototype.optimize = function() {
  return new CartesianProductNode(this.left.optimize(), this.right.optimize());
}


//// Joins

// ...
ASTNode.prototype.product = function(l, r) {
  return new CartesianProductNode(l, r);
}

ASTNode.prototype.join = function(f, l, r) {
  return new ThenNode(
    new ThenNode(
      new CartesianProductNode(l, r),
      new FilterNode(function(row) {
        return f(row.left, row.right);
      }
    )),
    new ApplyNode(function(row) {
      return Object.assign({}, row.left, row.right);
  }));
}


//// Optimizing joins

// ...
JoinNode.prototype = Object.create(ASTNode.prototype);

function JoinNode(f, l, r) {
  ASTNode.call(this, "Join");
  this.callback = function(row) {
    return f(row.left, row.right);
  };
  this.left = l;
  this.right = r;
  this.method = function(row) {
    return Object.assign({}, row.left, row.right);
  }
}

JoinNode.prototype.execute = function(table) {

    // Reuse existing methods
    var product = CartesianProductNode.prototype.execute.call(this, table);
    var filtered = FilterNode.prototype.execute.call(this, product);
    return ApplyNode.prototype.execute.call(this, filtered);
}

ASTNode.prototype.join = function(f, l, r) {
  return new JoinNode(f, l, r);
}
//// Join on fields

// ...
ASTNode.prototype.on = function(field) {
  return function(l, r) {
    return l[field] == r[field];
  }
}




//// Implement hash joins

// ...
HashJoinNode.prototype = Object.create(ASTNode.prototype);

function HashJoinNode(field, l, r) {
  ASTNode.call(this, "HashJoin");
  this.field = field;
  this.left = l;
  this.right = r;
}

function buildHash(table, field) {
  var hash = {};
  table.forEach(function(row) {
    if (!hash[row[field]]) {
      hash[row[field]] = []
    }
    hash[row[field]].push(row);
  });
  return hash;
}

HashJoinNode.prototype.execute = function(table) {
  var result = [];
  var left = this.left.execute(table);
  var right = this.right.execute(table);

  var leftHash = buildHash(left, this.field);
  var rightHash = buildHash(right, this.field);

  for (var key in leftHash) {
    if (rightHash[key]) {
      product(result, leftHash[key], rightHash[key])
    }
  }
  return result.map(function(row) {
    Object.assign({}, row.left, row.right);
  });
}


//// Optimize joins on fields to hash joins

// ...

ASTNode.prototype.on = function(field) {
  var f = function(l, r) {
    return l[field] == r[field];
  };
  f.on = field;
  return f;
}

ASTNode.prototype.join = function(f, l, r) {
  if (f.on) {
    return new HashJoinNode(f.on, l, r);
  } else {
    return new JoinNode(f, l, r);
  }
}

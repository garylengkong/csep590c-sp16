var antlr4 = require('antlr4/index');
var HandlebarsLexer = require('HandlebarsLexer').HandlebarsLexer;
var HandlebarsParser = require('HandlebarsParser').HandlebarsParser;
var HandlebarsParserListener = require('HandlebarsParserListener').HandlebarsParserListener;

function mangle(helper) {
    return `__$${helper}`;
}

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };
    this._usedHelpers = {};
    this._blocks = [];
    this.registerBlockHelper('each', function(ctx, body, list) {
        var result = '';
        for (var i=0; i<list.length; i++) {
            result += body(list[i]);
        }
        return result;
    });
    this.registerBlockHelper('if', function(ctx, body, condition) {
        if (condition) {
          return body(ctx);
        } else {
          return '';
        }
    });
    this.registerBlockHelper('with', function(ctx, body, prop) {
        return body(ctx[prop]);
    });
    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};

HandlebarsCompiler.prototype.pushScope = function () {
    this._bodyStack.push(`var ${this._outputVar} = "";\n`);
}

HandlebarsCompiler.prototype.popScope = function () {
    this._bodyStack[this._bodyStack.length - 1] += `return ${this._outputVar};\n`;
    return this._bodyStack.pop();
}

HandlebarsCompiler.prototype.compile = function (template) {
    this._usedHelpers = {};
    this._bodyStack = [];
    this.pushScope();

    var chars = new antlr4.InputStream(template);
    var lexer = new HandlebarsLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.document();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    return new Function(this._inputVar, this.popScope());
};

HandlebarsCompiler.prototype.append = function (expr) {
    this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`
};

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.append(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitLookupExpression = function (ctx) {
    ctx.source = `${this._inputVar}.${ctx.getText()}`;
};

HandlebarsCompiler.prototype.enterHelperExpression = function (ctx) {
    var helper = ctx.children[0].getText();
    if (!this._usedHelpers[helper]) {
        this._bodyStack[this._bodyStack.length - 1] += `var ${mangle(helper)} = ${this._helpers.expr[helper].toString()};\n`;
        this._usedHelpers[helper] = true;
    }
}

HandlebarsCompiler.prototype.exitHelperExpression = function (ctx) {
    var helper = ctx.children[0];
    ctx.source = `${mangle(helper)}(${this._inputVar}`;
    for (var i=1; i<ctx.children.length; i++) {
        var parameter = ctx.children[i];
        if (parameter.source) {
            ctx.source += `,${parameter.source}`;
        } else {
            ctx.source += `,${parameter.getText()}`;
        }
    }
    ctx.source += ')';
};

HandlebarsCompiler.prototype.exitParenExpression = function (ctx) {
    for (var i=0; i<ctx.children.length; i++) {
        var child = ctx.children[i];
        if (child.source) {
            ctx.source = child.source
        }
    };
};

HandlebarsCompiler.prototype.exitExpressionElement = function (ctx) {
    for (var i=0; i<ctx.children.length; i++) {
        var child = ctx.children[i];
        if (child.source) {
            this.append(child.source);
        }
    };
};

HandlebarsCompiler.prototype.enterBlock = function (ctx) {

    // Check non-matching blocks
    var blockStartElement = ctx.children[0];
    var blockEndElement = ctx.children[ctx.children.length - 1];
    var blockStart = blockStartElement.children[2].getText();
    var blockEnd = blockEndElement.children[2].getText();
    if (blockStart != blockEnd) {
      throw `Block start '${blockStart}' does not match the block end '${blockEnd}'.`;
    }

    // Define block helper functions
    if (!this._usedHelpers[blockStart] && this._helpers.block[blockStart]) {
        this._bodyStack[this._bodyStack.length - 1] += `var ${mangle(blockStart)} = ${this._helpers.block[blockStart].toString()};\n`;
        this._usedHelpers[blockStart] = true;
    }
}

HandlebarsCompiler.prototype.exitBlockElement = function (ctx) {
    this.pushScope();
}

HandlebarsCompiler.prototype.exitBlock = function (ctx) {
    var scope = new Function(this._inputVar, this.popScope());
    var blockStartElement = ctx.children[0];
    var helper = blockStartElement.children[2].getText();
    var block = `${mangle(helper)}(${this._inputVar},${scope.toString()}`;
    for (var i=3; i<blockStartElement.children.length - 1; i++) {
        var parameter = blockStartElement.children[i];
        if (parameter.source) {
            block += `,${parameter.source}`;
        } else {
            block += `,${parameter.getText()}`;
        }
    }
    block += ')';
    this.append(block);
}

exports.HandlebarsCompiler = HandlebarsCompiler;

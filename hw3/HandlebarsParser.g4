parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | block
    | commentElement
    ;

rawElement  : (BRACE? TEXT)+ BRACE?;

literal
    : INTEGER
    | FLOAT
    | STRING;

expression returns [source]
    : ID                                  # LookupExpression
    | ID (literal | expression)+          # HelperExpression
    | OPEN_PAREN expression CLOSE_PAREN   # ParenExpression
    ;

expressionElement
    : START expression END;

block
    : blockElement (rawElement | expressionElement | commentElement | block)* blockClosingElement;

blockElement
    : START BLOCK ID (literal | expression)* END;

blockClosingElement
    : START CLOSE_BLOCK ID END;

commentElement : START COMMENT END_COMMENT ;

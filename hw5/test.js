'use strict';

mocha.setup('bdd');

describe('Promise Library', function() {
  it('should be able to create and resolve promises using then', function(done) {
    var deferred = Q.defer();
    deferred.promise.then(function(val) {
      chai.expect(val).to.equal('a');
      done();
    });
    deferred.resolve('a');
  });

  it('should be able to create and chain promises using then', function(done) {
    var deferred = Q.defer();
    deferred.promise.then(function(val) {
      chai.expect(val).to.equal('a');
      return 'b';
    }).then(function(val) {
      chai.expect(val).to.equal('b');
      done();
    });
    deferred.resolve('a');
  });

  it('should be able to handle errors using then', function(done) {
    var deferred = Q.defer();
    deferred.promise.then(function(val) {
      return val;
    }, function(error) {
      chai.expect(error).to.equal('err');
      done();
    });
    deferred.reject('err');
  });

  it('should be able to handle errors using catch', function(done) {
    var deferred = Q.defer();
    deferred.promise.catch(function(error) {
      chai.expect(error).to.equal('error');
      done();
    });
    deferred.reject('error');
  });

  it('should be able to handle errors thrown in success handler', function(done) {
    var deferred = Q.defer();
    deferred.promise.then(function(val) {
      throw 'error';
    }).catch(function(error) {
      chai.expect(error).to.equal('error');
      done();
    });
    deferred.resolve('a');
  });

  it('should be able to handle errors thrown in error handler', function(done) {
    var deferred = Q.defer();
    deferred.promise.catch(function(val) {
      throw 'error';
    }).catch(function(error) {
      chai.expect(error).to.equal('error');
      done();
    });
    deferred.reject('a');
  });

  it('should be able to combine promises using Q.all', function(done) {
    var deferred1 = Q.defer();
    var deferred2 = Q.defer();
    Q.all([deferred1.promise, deferred2.promise]).then(function(val) {
      chai.expect(val[0]).to.equal('a');
      chai.expect(val[1]).to.equal('b');
      done();
    });
    deferred1.resolve('a');
    deferred2.resolve('b');
  });

  it('should have delay working correctly', function(done) {
    var start = new Date();
    Q.delay(100).then(function() {
      var end = new Date();
      chai.expect(end.getTime() - start.getTime()).to.be.at.least(100);
      done();
    });
  });

  it('should throw an error if a promise given to Q.timeout times out', function(done) {
    var start = new Date();
    var deferred = Q.defer();
    Q.timeout(deferred.promise, 100).catch(function(error) {
      var end = new Date();
      chai.expect(end.getTime() - start.getTime()).to.be.at.least(100);
      chai.expect(error).to.exist;
      done();
    });
  });

  it('should not throw an error if a promise given to Q.timeout does not time out', function(done) {
    var deferred = Q.defer();
    var timedOut = false;
    Q.timeout(deferred.promise, 100).catch(function(error) {
      // This shouldn't be executed
      timedOut = true;
    });
    setTimeout(function() {
      chai.expect(timedOut).to.be.true;
      done();
    }, 120);
  });

  it('should be able to wait for promise returned by the handler function', function(done) {
    // The second handler should be executed only when the
    // second promise is resolved
    var deferred1 = Q.defer();
    var deferred2;
    var fulfilled1 = false;
    var fulfilled2 = false;
    deferred1.promise.then(function() {
      chai.expect(fulfilled1).to.be.true;
      chai.expect(fulfilled2).to.be.false;
      deferred2 = Q.defer();
      return deferred2.promise;
    }).then(function() {
      chai.expect(fulfilled1).to.be.true;
      chai.expect(fulfilled2).to.be.true;
      done();
    });
    fulfilled1 = true;
    deferred1.resolve();
    fulfilled2 = true;
    deferred2.resolve();
  });

});

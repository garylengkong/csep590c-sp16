For homework 5 I will be implementing a toy version of Kris Kowal's Q promise
library. I decided to do this because this is the promise library used by
Angular 1 and I have been using the feature almost everyday without knowing how
it works. I hope to learn how it can be implemented using the design concepts I
learn in this class.

I thought about implementing the library by building an abstract syntax tree
just like homework 2, but it seems to be too complex for the task. The syntax
for the promise library looks more like streams in Rx, so followed the pattern
in hw4 instead.

I started off by building a global object which holds the defer method that contains the
deferred object which contains the promise object (thenable) and the resolve
function used to resolve the promise. Later on I decided to make the code more
object oriented by separating the definition of the promise object from the
deferred object. Once the defer method works correctly, I proceeded to make
utility functions such as all, delay, and timeout.

q.js includes the implementation of the promise library.
test.js contains the test cases used to test the functionality of the promise
library.
q.html is the page used to run and view the test results.

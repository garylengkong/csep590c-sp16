// The toy version of the Q promise library
function Q() {
}

Q.defer = defer;
Q.all = all;
Q.delay = delay;
Q.timeout = timeout;

// The main entry point for the promise library.
// This function creates a deferred object which contains
// a promise, a resolve function, and a reject function.
// Callbacks can be attached to the promise using the then
// or catch function. The resolve function can be used to
// resolve the promise and trigger the success handler callbacks.
// The reject function can be used to reject the promise and trigger
// the error handler callbacks.
function defer() {

  // The callbacks array is the place where we store all
  // our callbacks. A callback is an array which contains
  // a success handler for its first element, and an error
  // handler for its second element.
  var callbacks = [];
  var deferred = {};
  var promise = new Promise();

  // We need to define the add promise function here so that it
  // can access the callbacks array from this closure
  promise.add = function(callback) {
    callbacks.push(callback);
  }

  // Return a deferred object which contains the promise and
  // the resolve and reject functions for the
  return {
    promise: promise,
    resolve: function(value) {
      promise.result = value;
      if (value instanceof Promise) {
        value.then(function() {
          for (var i = 0; i < callbacks.length; i++) {
            callbacks[i][0](value.result);
          }
        });
      } else {
        for (var i = 0; i < callbacks.length; i++) {
          callbacks[i][0](value);
        }
      }
      promise.fulfilled = true;
    },
    reject: function(error) {
      for (var i = 0; i < callbacks.length; i++) {
        callbacks[i][1](error);
      }
    }
  };
}

// All function combines and waits for all promises in the array
// to complete before resolving and returning all values in an array
function all(promises) {
  var deferred = Q.defer();
  var unfulfilled = promises.length;
  var results = new Array(promises.length);
  promises.forEach(function(promise, i) {
    if (promise.fulfilled) {
      unfulfilled--;
    } else {
      promise.then(function(value) {
        results[i] = value;
        unfulfilled--;
        if (unfulfilled === 0) {
          deferred.resolve(results);
        }
      }, function(error) {
        deferred.reject(error);
      });
    }
  });
  return deferred.promise;
}

// Creates a promise which resolves in t miliseconds
function delay(t) {
  var deferred = Q.defer();
  setTimeout(deferred.resolve, t);
  return deferred.promise;
}

function timeout(promise, t) {
  var deferred = Q.defer();
  promise.then(function() {
    deferred.resolve();
  });
  delay(t).then(function() {
    deferred.reject(new Error("Timeout"));
  });
  return deferred.promise;
}

// The constructor for the promise object
function Promise() {
  this.fulfilled = false;
  this.result;
}

// A shorthand for calling then with no success handler and an
// errorHandler
Promise.prototype.catch = function(errorHandler) {
  return this.then(null, errorHandler);
}

// A method in a promise which adds the successHandler and errorHandler
// functions as a callback and returns a promise which can further
// be chained to produce another promise
Promise.prototype.then = function(successHandler, errorHandler) {
  var deferred = Q.defer();
  var callback = [];
  if (successHandler) {
    callback.push(function(value) {
      try {
        deferred.resolve(successHandler(value));
      } catch (error) {
        deferred.reject(error);
      }
    });
  } else {
    callback.push(function(value) {
      try {
        deferred.resolve(value);
      } catch (error) {
        deferred.reject(error);
      }
    });
  }

  if (errorHandler) {
    callback.push(function(error) {
      try {
        deferred.reject(errorHandler(error));
      } catch (error) {
        deferred.reject(error);
      }
    });
  } else {
    callback.push(function(error) {
      try {
        deferred.reject(error);
      } catch (error) {
        deferred.reject(error);
      }
    });
  }
  this.add(callback);
  return deferred.promise;
}

function get(url) {
  var deferred = Q.defer();

  $.ajax(url, {
    success: function(data) {
      deferred.resolve(data);
    },
    error: function() {
      deferred.reject('URL request failed');
    }
  });
  return deferred.promise;
}

function appendTable(data) {
  var table = $('<table></table>');
  for (var i=0; i<data.length; i++) {
    var row = $('<tr></tr>');
    for (var j=0; j<data[i].length; j++) {
      row.append('<td>' + data[i][j] + '</td>');
    }
    table.append(row);
  }
  $('#example').append(table);
  $('#example').append('<br>');
}

var url1 = 'http://api.census.gov/data/timeseries/asm/state?get=NAICS_TTL,EMP,GEO_TTL&for=state:*&time=2012&NAICS=31-33';
var url2 = 'http://api.census.gov/data/timeseries/asm/state?get=NAICS_TTL,EMP,GEO_TTL&for=state:*&time=2013&NAICS=31-33';
var url3 = 'http://api.census.gov/data/timeseries/asm/state?get=NAICS_TTL,EMP,GEO_TTL&for=state:*&time=2014&NAICS=31-33';

get(url1)
.then(function(data) {
  appendTable(data);
  return get(url2);
})
.then(function(data) {
  appendTable(data);
  return get(url3);
})
.then(function(data) {
  appendTable(data);
});

// Without promise
/*
$.ajax(url1).done(function(data) {
  appendTable(data);
  $.ajax(url2).done(function(data) {
    appendTable(data);
    $.ajax(url3).done(function(data) {
      appendTable(data);
    });
  });
});
*/

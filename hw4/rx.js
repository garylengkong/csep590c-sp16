
function Stream() {
    this.callbacks = [];
}

Stream.prototype.subscribe = function(callback) {
    this.callbacks.push(callback);
};

Stream.prototype._push = function(value) {
    for (var i=0; i<this.callbacks.length; i++) {
      this.callbacks[i](value);
    }
};

Stream.prototype._push_many = function(values) {
    var self = this;
    values.map(function(value) {
      self._push(value);
    });
};

Stream.prototype.first = function() {
    var newStream = new Stream();
    newStream.isFirst = true;
    this.subscribe(function(x) {
        if (newStream.isFirst) {
            newStream._push(x);
            newStream.isFirst = false;
        }
    });
    return newStream;
};

Stream.prototype.map = function(mapFunction) {
    var newStream = new Stream();
    this.subscribe(function(x) {
        newStream._push(mapFunction(x));
    });
    return newStream;
};

Stream.prototype.filter = function(filterFunction) {
    var newStream = new Stream();
    this.subscribe(function(x) {
        if (filterFunction(x)) {
            newStream._push(x);
        }
    });
    return newStream;
};

Stream.prototype.flatten = function() {
    var newStream = new Stream();
    this.subscribe(function(arr) {
        arr.map(function(x) {
            newStream._push(x);
        });
    });
    return newStream;
}

Stream.prototype.join = function(stream) {
    var newStream = new Stream();
    this.subscribe(function(x) {
        newStream._push(x);
    });
    stream.subscribe(function(x) {
        newStream._push(x);
    });
    return newStream;
}

Stream.prototype.combine = function() {
    var newStream = new Stream();
    this.subscribe(function(stream) {
        stream.subscribe(function(x) {
          newStream._push(x);
        });
    });
    return newStream;
}

Stream.prototype.zip = function(zipStream, zipFunction) {
    var newStream = new Stream();
    var lastA, lastB;
    this.subscribe(function(x) {
        lastA = x;
        if (lastA != undefined && lastB != undefined) {
            newStream._push(zipFunction(lastA, lastB));
        }
    });
    zipStream.subscribe(function(y) {
        lastB = y;
        if (lastA != undefined && lastB != undefined) {
            newStream._push(zipFunction(lastA, lastB));
        }
    });
    return newStream;
}

Stream.timer = function(N) {
    var newStream = new Stream();
    setInterval(function() {
        newStream._push(new Date());
    }, N);
    return newStream;
}

Stream.dom = function(element, eventname) {
    var newStream = new Stream();
    element.on(eventname, function(event) {
        newStream._push(event);
    });
    return newStream;
};

Stream.prototype.throttle = function(N) {
    var newStream = new Stream();
    var throttled = true;
    this.subscribe(function(x) {
        if (throttled) {
            throttled = false;
            setTimeout(function() {
                newStream._push(x);
                throttled = true;
            }, N);
        }
    });
    return newStream;
};

Stream.url = function(url) {
    var newStream = new Stream();
    $.get(url, function(data) {
        newStream._push(data);
    }, 'json');
    return newStream;
};

Stream.prototype.latest = function() {
    var newStream = new Stream();
    var latest;
    this.subscribe(function(stream) {
        latest = stream;
        stream.subscribe(function(x) {
            if (stream == latest) {
                newStream._push(x);
            }
        });
    });
    return newStream;
};

Stream.prototype.unique = function(f) {
    var newStream = new Stream();
    var hashMap = {};
    this.subscribe(function(x) {
        var hashCode = f(x);
        if (!hashMap[hashCode]) {
            newStream._push(x);
            hashMap[hashCode] = true;
        }
    });
    return newStream;
};

// PART 1 HERE

var FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

window.WIKICALLBACKS = {}

function WIKIPEDIAGET(s, cb) {
    $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        jsonp: "callback",
        data: {
            action: "opensearch",
            search: s,
            namespace: 0,
            limit: 10,
        },
        success: function(data) {cb(data[1])},
    })
}

$(function() {
    // PART 2 INTERACTIONS HERE
    // Display current time every 1 second
    var timer = Stream.timer(1000);
    timer.subscribe(function(time) {
        $('#time').text(time);
    });

    // Save number of times button is pressed
    var clicks = 0;
    var $clicks = $('#clicks');
    Stream.dom($('#button'), 'click')
    .subscribe(function(event) {
        clicks++;
        $clicks.text(clicks);
    });

    // Throttle mouse move events for 1 second
    Stream.dom($('#mousemove'), 'mousemove')
    .throttle(1000).subscribe(function(event) {
        $('#mouseposition').text(event.pageX + ', ' + event.pageY);
    });

    var $search = $('#wikipediasearch');
    var $suggestions = $('#wikipediasuggestions');

    // Use latest() to get only data from the latest stream
    Stream.dom($search, 'input')
    .throttle(100).map(function(event) {
        var searchStream = new Stream();
        WIKIPEDIAGET(event.target.value, function(data) {
            searchStream._push(data);
        });
        return searchStream;
    }).latest().subscribe(function(data) {
        $suggestions.empty();
        data.map(function(title) {
            $suggestions.append($('<li></li>').text(title));
        });
    });

    // Food for thought
    // I think this an optimization for this is probably to combine the map
    // and latest function if the map function produce a stream,
    // and discard streams that are not the latest instead of keeping
    // them around and check if they are the latest or not.

    var $fireevents = $('#fireevents');
    var $firesearch = $('#firesearch');

    // Save unique calls in an array to reload
    // results when firesearch is changed
    var uniqueCalls = [];

    // Save timer stream so we can trigger it right away
    var timer = Stream.timer(60000);
    var uniqueCallStream = timer
        .map(function() { return Stream.url(FIRE911URL); })
        .combine()
        .flatten()
        .unique(function(x) {
            return x.id
        });

    // Save unique calls
    uniqueCallStream.subscribe(function(call) {
        uniqueCalls.push(call);
    });

    // Filter based on firesearch and add to list
    uniqueCallStream
        .filter(function(call) {
            var search = $firesearch.val();
            return call[3479077].match(new RegExp(search, 'i'));
        })
        .subscribe(function(call) {
            $fireevents.append($('<li></li>').text(call[3479077]));
        });

    // Trigger first 911 call request
    timer._push();

    // Listen for changes in firesearch and reload list
    Stream.dom($firesearch, 'input')
        .subscribe(function(event) {
            $fireevents.empty();
            var search = event.target.value;
            var stream = new Stream();
            stream.filter(function(call) {
                return call[3479077].match(new RegExp(search, 'i'));
            })
            .subscribe(function(call) {
                $fireevents.append($('<li></li>').text(call[3479077]));
            });
            stream._push_many(uniqueCalls);
        });
});

